import { Given, When, Then, And } from "@wdio/cucumber-framework";
import Page from "../pageobjects/pages/page.js";
import LoginPage from "../pageobjects/pages/login.page.js";
import { expect } from "@wdio/globals";

const page = new Page();
const loginPage = new LoginPage();

Given(/^I had registered before$/, async () => {
  await page.open("");
  await browser.maximizeWindow();
});

When(/^I click login button$/, async () => {
  await loginPage.loginComponent.loginBtn.click();
});

And(/^I type a valid username and password $/, async () => {
  await loginPage.loginComponent.login("", "");
});

Then(/^I should successfully sign up$/, async () => {
  await expect(browser).toHaveUrl("https://trello.com/u/janedoe406/boards");
});
